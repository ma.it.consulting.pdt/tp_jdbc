package com.devemg;

import com.devemg.data.impl.ProductDaoImpl;
import com.devemg.data.utils.MysqlConnection;
import com.devemg.data.dao.ProductDAO;
import com.devemg.data.entities.Product;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Menu {
    private final ProductDAO productJDBC;

    public Menu() {
        this.productJDBC = new ProductDaoImpl();
    }

    public int showMenu(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("-----------------------------------------------------");
        System.out.println("\t\t Liste de produit ");
        System.out.println("------------------------------------------------------\n");
        System.out.println("1. Enregister un produit");
        System.out.println("2. Afficher la liste des produits");
        System.out.println("3. Afficher un produit");
        System.out.println("4. Mise à jour d'un produit");
        System.out.println("5. Supprimer un produit");
        System.out.println("6. Exit");
        return scanner.nextInt();
    }

    public void createProduct(){
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Allons créer un produit!");
            System.out.println("Donnez moi toutes les informations concernant le produit.");
            System.out.println("Nom: ");
            String name = scanner.nextLine();
            System.out.println("Prix: ");
            double price = scanner.nextDouble();
            System.out.println("Quantité: ");
            int qty = scanner.nextInt();
            scanner.nextLine();
            System.out.println("Description: ");
            String desc = scanner.nextLine();

            int insert = this.productJDBC.insert(new Product(name,price,qty,desc));
            if(insert > 0) {
                System.out.println("Produit crée !");
            }else {
                System.out.println("oh no! Le produit n'a pu être crée !");
            }
            scanner.nextLine();
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }catch(InputMismatchException ex) {
            System.out.println("oh no! Votre saisie est incorrecte.\n Veuillez recommencer.");
            scanner.nextLine();
            scanner.nextLine();
        }
    }

    public void showAll() {
        try {
            System.out.println("Voici la liste !");
            List<Product> products = this.productJDBC.select();
            products.forEach(System.out::println);
            new Scanner(System.in).nextLine();
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void showOne() {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Voyons le produit!");
            System.out.println("Donnez moi l'id du produit ");
            int id = scanner.nextInt();
            Product product = productJDBC.select(id);
            scanner.nextLine(); //consume \n
            if(product != null) {
                System.out.println(product);
            }else {
                System.out.println("Produit non trouvé ");
            }
            scanner.nextLine();
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }catch (InputMismatchException ex) {
            System.out.println("oh no! Votre saisie est incorrecte.\\n Veuillez recommencer.");
            scanner.nextLine();
            scanner.nextLine();
        }
    }

    public void update() {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Allons modifier le produit !");
            System.out.println("Donnez moi le produit à modifier et les éléments à modifier.");
            System.out.println("Id du produit: ");
            int id = scanner.nextInt();
            scanner.nextLine(); //consume \n
            Product product = productJDBC.select(id);
            if(product != null) {
                System.out.print("Nom["+product.getName()+"]:");
                String name = scanner.nextLine();
                System.out.print("Prix["+product.getprice()+"]:");
                String price = scanner.nextLine();
                System.out.print("Quantité["+product.getQuantity()+"]:");
                String qty = scanner.nextLine();
                System.out.print("Description["+product.getDescription()+"]:");
                String desc = scanner.nextLine();
                if(!name.equals("")){
                    product.setName(name);
                }
                if(!desc.equals("")){
                    product.setDescription(desc);
                }
                if(tryParseDouble(price)){
                    product.setprice(Double.parseDouble(price));
                }
                if(tryParseInt(qty)){
                    product.setQuantity(Integer.parseInt(qty));
                }
                int result = productJDBC.update(product);
                if(result > 0) {
                    System.out.println("produit mis à jour!");
                }else {
                    System.out.println("produit non mis à jour");
                }
            }else {
                System.out.println("Produit non trouvé");
            }
            scanner.nextLine();
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }catch (InputMismatchException ex) {
            System.out.println("oh no! Votre saisie est incorrecte.\\n Veuillez recommencer.");
            scanner.nextLine();
            scanner.nextLine();
        }
    }

    public void delete() {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Allons supprimer un produit!");
            System.out.println("Donnez moi l'id du produit à supprimer : ");
            int id = scanner.nextInt();
            int result = productJDBC.delete(id);
            scanner.nextLine(); //consume \n
            if(result > 0) {
                System.out.println("Produit supprimé!");
            }else {
                System.out.println("Produit non trouvé");
            }
            scanner.nextLine();
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }catch (InputMismatchException ex) {
            System.out.println("oh no! Votre saisie est incorrecte.\\n Veuillez recommencer.");
            scanner.nextLine();
            scanner.nextLine();
        }
    }

    /*public void updateDatabase() {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Allons modifier le produit!");
            System.out.println("Donnez moi les données à modifier.");
            System.out.print("Host["+ MysqlConnection.getHost() +"]:");
            String host = scanner.nextLine();
            System.out.print("Port["+MysqlConnection.getPort()+"]:");
            String port = scanner.nextLine();
            System.out.print("Database["+ MysqlConnection.getDatabase() +"]:");
            String database = scanner.nextLine();
            System.out.print("User["+MysqlConnection.getUser()+"]:");
            String user = scanner.nextLine();
            System.out.print("Password:");
            String password = scanner.nextLine();
            // update info
            if(!host.equals("")){
                MysqlConnection.setHost(host);
            }
            if(tryParseInt(port)){
               MysqlConnection.setPort(Integer.parseInt(port));
            }
            if(!database.equals("")){
                MysqlConnection.setDatabase(database);
            }
            if(!user.equals("")){
                MysqlConnection.setDatabaseUser(user);
            }
            //password can be empty
            MysqlConnection.setDatabasePassword(password);
            //reload connection with database
            MysqlConnection.reloadStringConnection();
            // show changes
            //System.out.println(MysqlConnection.getConnectionString());
            //System.out.println(MysqlConnection.getUser());
            //System.out.println(MysqlConnection.getPassword());
            System.out.println("database information was updated!");
            scanner.nextLine();
        }catch (InputMismatchException ex) {
            System.out.println("oh no! the input was type wrong.\n come back and try again.");
            scanner.nextLine();
            scanner.nextLine();
        }
    }
*/
    public boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean tryParseDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
