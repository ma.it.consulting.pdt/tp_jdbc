package com.devemg;

public class Main {
    private static final int LAST_OPTION_MENU = 7;
    public static void main(String[] args) {
        Menu menu = new Menu();
        int option;
        do {
            option = menu.showMenu();
            switch (option) {
                case 1 -> // create product
                        menu.createProduct();
                case 2 -> //list products
                        menu.showAll();
                case 3 -> //see single product
                        menu.showOne();
                case 4 -> //update product
                        menu.update();
                case 5 -> //delete product
                        menu.delete();
                case 6 -> //config database
                        menu.updateDatabase();
                case LAST_OPTION_MENU -> //exit
                        System.out.println("bye!");
                default -> System.out.println("Option " + option + " not found");
            }
        } while (option!=LAST_OPTION_MENU);
    }


}
